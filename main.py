import csv
import re

dnsLog = open('dns.log')
hosts = open('hosts_all1')
dns = csv.reader(dnsLog, delimiter="\x09")

r = re.compile('[^a-zA-Z.]')
unwanted = []
m = 0
n = 0

for l in hosts.readlines():
    l = re.sub(r'\[[^][]*\]', '', l)
    if l != '\n':
        unwanted.append(r.sub('', l))

for st in dns:
    try:
        n += 1
        if st[9] in unwanted:
            print(st[9])
            m += 1
    except IndexError:
        pass


print("Нежелательный трафик = "+str(m * 100.0 / n) + "%")

dnsLog.close()
hosts.close()
